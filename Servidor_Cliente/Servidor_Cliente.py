# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 07:47:01 2022

@author: danie
"""

#!/usr/bin/env python3.8
import socket, time
from math import sqrt

SERVER_ADDRESS = ""
SERVER_PORT = 9797

s = socket.socket()
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((SERVER_ADDRESS, SERVER_PORT))
s.listen(5)

print("ESPERANDO CLIENTE %s. " %
      str((SERVER_ADDRESS, SERVER_PORT)))

while True:
    c, addr = s.accept()
    print("\n Cliente %s" % str(addr))

    while True:

        data = c.recv(2048)
        if not data:
            print("Datos enviados. Resetting")
            break

        print(data.decode())

        analisis = data.decode()
        separar = analisis.split(',')

        operacion = separar[0]
        factor1 = separar[1]
        factor2 = separar[2]
        factor3 = separar[3]

        if operacion == 'Cuadratica':
            A = int(factor1)
            B = int(factor2)
            C = int(factor3)
            x1 = 0
            x2 = 0
            if ((B**2)-4*A*C) < 0:
                x1 = (-B+sqrt(B**2-(4*A*C)))/(2*A)
                x2 = (-B-sqrt(B**2-(4*A*C)))/(2*A)
                data3 = 'operacion:'+','+str(x1)+','+str(x2)+','+'FIN'
            else:
                x1 = (-B+sqrt(B**2-(4*A*C)))/(2*A)
                x2 = (-B-sqrt(B**2-(4*A*C)))/(2*A)
                data3 = 'operacion:'+','+str(x1)+','+str(x2)+','+'FIN'
        


        data3 = data3.encode()

        c.send(data3)
        print('ENVIANDO RESPUESTA')
        #break
        time.sleep(1)

    c.close()