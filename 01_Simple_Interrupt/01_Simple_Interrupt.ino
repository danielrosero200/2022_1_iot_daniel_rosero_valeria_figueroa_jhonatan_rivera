struct Button {
  // Pin donde se da la interrupción
  const uint8_t PIN;
  // El contador de número de veces que se presiona el botón
  uint32_t numberKeyPresses;
  // Se maneja dos estados TRUE/FALSE
  bool pressed;
};

Button button1 = {18, 0, false};

void IRAM_ATTR isr() {
  // Se incrementa el contador
  button1.numberKeyPresses += 1;
  // El estado es presionado
  button1.pressed = true;
}

void setup() {
  Serial.begin(115200);
  // Habilita el resistor interno para manejo de switch
  pinMode(button1.PIN, INPUT_PULLUP);
  attachInterrupt(button1.PIN, isr, FALLING);
}

void loop() {
  if (button1.pressed) {
      Serial.printf("Button 1 has been pressed %u times\n", button1.numberKeyPresses);
      button1.pressed = false;
  }

  //Detach Interrupt after 1 Minute
  static uint32_t lastMillis = 0;
  if (millis() - lastMillis > 60000) {
    lastMillis = millis();
    detachInterrupt(button1.PIN);
  Serial.println("Interrupt Detached!");
  }
}
